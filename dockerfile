# Base image
FROM nginx:1.15.8-alpine

# Copy all content of site documentation folder to nginx container
COPY ./site /usr/share/nginx/html
