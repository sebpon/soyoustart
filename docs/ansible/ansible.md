# Ansible tips & tricks

This page presents some tips & tricks when writing Ansible playbooks

## ansible-vault

To encrypt a single variable in a playbook

```bash
ansible-vault --vault-id=vault_file_name encrypt_string secret_string_to_encrypt
```

Use option `--vault-id` as the other options like `--ask-vault-pass` will be deprecated. vault-id enables the use of multiple vaults to protect different kind of data inside the same playbook.

## Reboot a server

!!! tips
    If you're using Ansible version > 2.7 the life is easy as you can use the `reboot` module which is new from version 2.7.

Here is a technique that avoids errors running playbooks where you put a `shell` module with a `reboot` command.

The problem is that when doing a reboot inside an Ansible playbook, your Ansible controll server will loose connection with the remote managed server, hence throwing an error like the one below :

```bash
TASK [Reboot immediately  **********************************************************************************************
fatal: [10.0.100.44]: UNREACHABLE! => changed=false
  msg: |-
    Failed to connect to the host via ssh: Shared connection to 10.0.100.44 closed.
  unreachable: true

PLAY RECAP *************************************************************************************************************
10.0.100.44                : ok=4    changed=2    unreachable=1    failed=0
```

You have then to restart your playbook to continue with the configuration.

Ansible provides 2 tools to avoid this problem: `async` and `wait_for_connection`.  

```yaml
---
- name: Do something that requires a reboot when it results in a change.
  ...
  register: task_result

- name: Reboot immediately if there was a change.
  shell: "sleep 5 && reboot"
  async: 1
  poll: 0
  when: task_result is changed

- name: Wait for the reboot to complete if there was a change.
  wait_for_connection:
    connect_timeout: 20
    sleep: 5
    delay: 5
    timeout: 300
  when: task_result is changed

...
```

`async` tells Ansible to not block the connection for its value in seconds and uses poll to define a polling interval to check if the task is executed. In this case, we tell Ansible to issue the shell command to reboot in async mode and not to poll for the result of it. Normal because otherwise the connection will be broken and our playbook will fail.  
We also put a sleep before the reboot to allow Ansible to correctly manage the connection for async polling.

`wait_for_connection` is a module that waits for a target server to be available for connection.

To avoid unwanted reboot (when the system is not changed), use `task_result` variable to ensure running the reboot tasks only when needed.