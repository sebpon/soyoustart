# pfsense

pfsense is an appliance that acts as a firewall / reverse-proxy / vpn.

It enables one access point to / from outside networks (internet) to protect our infrastructure.

## Configuration of the VM

Running on VMware ESX in soyoustart, the standard offer enables only one public IP address that is assigned to the ESX host itself. The recommended way of deploying pfsense is to order an additional public IP address and to assign it as well as the corresponding mac address to the pfsense appliance.

## pfSense configurations

### Define default gateway

Most of the guides found on internet like [this one](https://www.it-connect.fr/pfsense-2-4-3-ip-failover-sur-un-esxi-chez-ovh/) will ask you to run shell command to add a default gateway and install the `shellcmd` package to run the same command when pfsense appliance reboot (otherwise you'll loose internet connectivity). Since pfsense 2.3, there is another way to accomplish this without the shellcmd "hack".

Logon to the pfsense configuration interface and go to `System -> Routing -> Gateways`.
Click add new gateway:

- On the config page that opens for the new gateway, go to the bottom, look, find and click `Show advanced`.
- Check the last option `Use non-local gateway through interface specific route.`
- Now fill the IP address data of the OVH gateway (in this example 1.2.3.254)
- Check the option `This will select the above gateway as the default gateway.`
- Fill the fields `Name` and `Description` accordingly...
- Save and Apply


### Install additional packages

Go to `System -> Package Manager`

Install the following packages

- `Open-VM-Tools`: installs VMware tools in the pfSense appliance.
- `openvpn-client-export`: enables to export a preconfigured OpenVPN client for Windows and Mac users.
- `pfBlockerNG`: enables the blocking of trafic coming from black listed IP addresses.

### Define some aliases

Go to `Firewall -> Aliases -> All`

```bash
httpPort 80,443,8000,8080 (etc)
```

Define all ports that should be accessed by external networks to enable web applications without VPN.

```bash
rProxy 192.168.20.10
```

Define the internal ip address that will be assigned to the VM that will act as a reverse proxy for all connections made on ports liste in alias `httpPort`.

```bash
LetsEncryptOut outbound2.letsencrypt.org
```

Define the DNS name used by Let's Encrypt ACME protocol to check that the TLS certificate you require is bound to a host that you manage. It is used by `Traefik` reverse proxy to automagically manage the TLS certificates.

### Define NATing rules

Go to `Firewall -> NAT`

Port forward

- Interface: WAN
- Protocol: TCP
- Source Address: *
- Source Ports: *
- Dest Address: WAN address
- Dest Ports: httpPort (alias)
- NAT Ip: rProxy (alias)
- NAT Ports: httpPort (alias)

Outbound

- Interface: WAN
- Source: any
- Source Port: *
- Destination: *
- Destination Port: *
- NAT address: WAN address
- NAT Port: *

> Note
> We could try to use the automatic outbound NAT rule generation option ...

## Install and configure pfBlockerNG addon

pfBlocker is a plugin that enables the blocking of known IP addresses 