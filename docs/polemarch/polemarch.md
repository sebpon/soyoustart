# Polemaarch

Polemarch is an open source GUI for Ansible that enables to manage

- projects
- inventories
- playbooks

It also has an API to trigger ansible playbooks from external sources.

## Installation

To create the polemarch database use the following script

```bash
sudo -H mysql <<QUERY_INPUT
SET @@global.innodb_large_prefix = 1;
create user polemarch identified by 'sfrzT3v85E2kbENMSguN!';
create database polemarchdb default CHARACTER set utf8 default COLLATE utf8_general_ci;
grant all on polemarchdb.* to 'polemarch'@'%';
QUERY_INPUT
```

Create a directory /data/polemarch on the SWARM host

```bash
sudo mkdir -p /data/polemarch
```

Install required packages

```bash
sudo yum install python36u python36u-devel python36-virtualenv openssl-devel libyaml-devel krb5-devel krb5-libs openldap-devel mysql-devel git sshpass gcc
```

Create user polemarch

```bash
sudo useradd --user-group --create-home --shell /bin/bash polemarch
```

Create virtual environment in Pytho 3.6

```bash
sudo py3-virtualenv --python=python3.6 /opt/polemarch
sudo chown -R polemarch:polemarch /opt/polemarch
sudo -u polemarch -i
source /opt/polemarch/bin/activate
```

Install polemarch and mysqlclient through pip as user polemarch with virtualenv sourced (same session as last actions)

```bash
pip install -U polemarch mysqlclient
```

Create the /etc/polemarch directory


Create the file /etc/polemarch/settings.conf with the following content


Run 

```bash
polemarchctl migrate
Operations to perform:
  Apply all migrations: admin, auth, authtoken, contenttypes, django_celery_beat, main, sessions
Running migrations:
  No migrations to apply.
```

Start polemarch web server

```bash
polemarchctl webserver
```

Open the port 8080 for http

```bash
# firewall-cmd --permanent --zone=public --add-port=8080/tcp
# firewall-cmd --reload
```

## Docker deployment

Note: deployment works but impossible to login into web interface (keeps getting back to login page) with no error messages ...

Deploy the stack PM (for polemarch) as user `seb`

```bash
cd ~/polemarch
sudo docker stack deploy -c polemarch-stack.yml PM
```

Logon in the polemarch container to change admin user

```bash
sudo docker container ls
CONTAINER ID        IMAGE                           COMMAND                  CREATED             STATUS              PORTS                          NAMES
504f5e39adfc        vstconsulting/polemarch:1.2.0   "/opt/polemarch/bin/…"   8 minutes ago       Up 8 minutes        8080/tcp                       PM_polemarch.1.s414bvxc9yuhyqzdl1hemhrha
3d702b483b51        redis:5.0.5-alpine              "docker-entrypoint.s…"   8 minutes ago       Up 8 minutes        6379/tcp                       PM_redis-pm.1.mhg00dwepm254nt9r1otjw59m
daaeb19d95ac        couchdb:2.3.0                   "tini -- /docker-ent…"   8 days ago          Up 8 days           4369/tcp, 5984/tcp, 9100/tcp   swarmpit_db.1.rds401s5xezgbfg24gyvne63g
cb7baa30a7dc        swarmpit/swarmpit:1.7           "java -jar swarmpit.…"   8 days ago          Up 8 days           8080/tcp                       swarmpit_app.1.levwile4hro2b7gionllj17rm
ddda5605edaa        swarmpit/agent:2.1              "./agent"                8 days ago          Up 8 days           8080/tcp                       swarmpit_agent.qd4nv2wmhp609scw1s8kmbi6h.s73uz236fbr5ty94shrs5scdi
b01c5c2553cc        grafana/grafana:6.2.2           "/run.sh"                8 days ago          Up 8 days           3000/tcp                       TICK_grafana.1.9jqinecjvh2nndugj5hs6vxcm
e354bb71017c        chronograf:1.7.5-alpine         "/entrypoint.sh chro…"   5 months ago        Up 5 months         8888/tcp                       TICK_chronograf.1.zq6ctep5458hxnzh641inxsim
d57256578626        influxdb:1.7.2-alpine           "/entrypoint.sh infl…"   5 months ago        Up 5 months         8086/tcp                       TICK_influxdb.1.xjjidbym7wlpxn137r9ar9mvv

sudo docker container exec -it 504f5e39adfc /bin/bash
bash-4.4# ./polemarchctl createsuperuser
Username (leave blank to use 'root'): seb
Email address: sebastien.pondichy@gmail.com
Password:
Password (again):
Superuser created successfully.
exit
```