# Graylog

## TLS setup

Use the XCA utility to generate a key pair + TLS certificate for the graylog host. The certificate must be signed by the Intermediate CA of seblab.local. It must contain the following subject alternate names :

* DNS Name: graylog
* DNS Name: graylog.seblab.local
* IP Address: 192.168.20.20

From XCA, export the graylog private key as PEM encrypted file (and give a password) and the graylog certificate as PEM crt file. Be aware that the PKCS8 export formats of XCA for the private key does not work for Graylog ! 

Copy both certificate and key PEM files to graylog host on `/etc/graylog/tls/`

Convert the graylog key PEM file to PKCS8 PEM format with `openssl`

```bash
# openssl pkcs8 -topk8 -inform PEM -outform PEM -in graylog.pem -out graylog.key 
```

Use ansible to configure TLS for Graylog or configure it manually.

Edit `/etc/graylog/server/server.conf` and adapt the following parameters

```bash
http_bind_address: 192.168.20.20:9443
http_publish_uri: https://192.168.20.20:9443
http_enable_tls: true
http_tls_cert_file: /etc/graylog/tls/graylog.crt
http_tls_key_file: /etc/graylog/tls/graylog.key
http_tls_key_password: SuperSecretPassword
```

Restart graylog-server service.

Don't forget to open the new HTTPS port through firewalld

```bash
# firewall-cmd --permanent --zone=public --add-port=9443/tcp
# firewall-cmd --reload
```