# GoCD

# Startup script

The startup / shutdown scripts for go-server are located in /usr/share/go-server.

# Enable user authentication

Generate a user and a bcrypt password hash in a file named /var/lib/go-server/go-passwd

```bash
mkdir -p /var/lib/go-server/security && cd "$_"
htpasswd -c -B /var/lib/go-server/security/go-passwd user
```

!!! info
    htpasswd is part of the httpd-tools package

Configure restricted ACLs to limit access to the file to only the go user.

```bash
chown -R go:go /var/lib/go-server/security
chmod -R 600 /var/lib/go-server/security
```

Configure the password file as described in GoCD documentation: https://docs.gocd.org/current/configuration/dev_authentication.html
