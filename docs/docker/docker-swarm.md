# Docker SWARM mode

Even for a single node, `docker swarm` is a nice way of making your containers more resilient.  
`docker swarm` is a container scheduler that is shipped as part of `docker` product.

## Docker swarm core concepts

### Swarm

A swarm consists of mone or more docker hosts that will act together to manage and orchestrate docker containers.

### Node

A node is a host where a docker instance participating to a swarm is running.

### Manager

`docker swarm` managers are docker hosts that will manage the swarm cluster

### Worker

`docker swarm` workers are docker hosts that will run the swarm services

### Service

A service is the definition of the tasks to execute on the manager or worker nodes. It is the central structure of the swarm system and the primary root of user interaction with the swarm.

When you create a service, you specify which container image to use and which commands to execute inside running containers.

For global services, the swarm runs one task for the service on every available node in the cluster.

A task carries a Docker container and the commands to run inside the container. It is the atomic scheduling unit of swarm. Manager nodes assign tasks to worker nodes according to the number of replicas set in the service scale. Once a task is assigned to a node, it cannot move to another node. It can only run on the assigned node or fail.

## Initialize the swarm

`docker swarm init`

## Some basic commands

`docker info` displays the swarm status amongst other things.

`docker node ls` displays the docker nodes participating to the swarm.

`docker service ls` displays the services deployed in the swarm.

For more info see the documentation to deploy influxdb, chronograf and grafana into a swarm.

## Update a docker swarm service

If you want to update the version of an already deployed and running service in docker swarm, use the following command (example: update the grafana service from 5.4.2 to 6.0.0)

```bash
[root@swarm01 ~]# docker service ls
ID                  NAME                  MODE                REPLICAS            IMAGE                        PORTS
ibt5co6z63wy        TICK_chronograf       replicated          1/1                 chronograf:1.7.5-alpine      *:8888->8888/tcp
e2mobqdxs60s        TICK_grafana          replicated          1/1                 grafana/grafana:5.4.2        *:3000->3000/tcp
ugcduaiu294p        TICK_influxdb         replicated          1/1                 influxdb:1.7.2-alpine        *:8086->8086/tcp
okvkvaiw9vu1        portainer_agent       global              1/1                 portainer/agent:latest
r4zdpirqtnaz        portainer_portainer   replicated          1/1                 portainer/portainer:latest   *:9000->9000/tcp
[root@swarm01 ~]# docker service update --image grafana/grafana:6.0.0 TICK_grafana
TICK_grafana
overall progress: 1 out of 1 tasks
1/1: running   [==================================================>]
verify: Service converged
[root@swarm01 ~]# docker service ls
ID                  NAME                  MODE                REPLICAS            IMAGE                        PORTS
ibt5co6z63wy        TICK_chronograf       replicated          1/1                 chronograf:1.7.5-alpine      *:8888->8888/tcp
e2mobqdxs60s        TICK_grafana          replicated          1/1                 grafana/grafana:6.0.0        *:3000->3000/tcp
ugcduaiu294p        TICK_influxdb         replicated          1/1                 influxdb:1.7.2-alpine        *:8086->8086/tcp
okvkvaiw9vu1        portainer_agent       global              1/1                 portainer/agent:latest
r4zdpirqtnaz        portainer_portainer   replicated          1/1                 portainer/portainer:latest   *:9000->9000/tcp
```

The prior version of the task is still referenced

```bash
[root@swarm01 ~]# docker service ps TICK_grafana
ID                  NAME                 IMAGE                   NODE                   DESIRED STATE       CURRENT STATE            ERROR               PORTS
w6g17jlaf2h8        TICK_grafana.1       grafana/grafana:6.0.0   swarm01.seblab.local   Running             Running 4 minutes ago
j9qsd1oyc2yo         \_ TICK_grafana.1   grafana/grafana:5.4.2   swarm01.seblab.local   Shutdown            Shutdown 5 minutes ago
```

If the stack was deployed with a docker-compose file, do not forget to update it also to reflect the changes in case you need to redeploy the stack on a fresh infrastructure.

## Swarm Upgrade

See [Bret Fisher's notes on Docker Swarm upgrade](https://gist.github.com/BretFisher/85ebcacd9c295b556d6b8a68af20174d)


