# swarmpit

swarmpit is an interface to manage a docker swarm cluster.

## Installation

Follow the instructions on https://github.com/swarmpit/swarmpit#installation

```bash
docker run -it --rm \
  --name swarmpit-installer \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  swarmpit/install:1.6

Unable to find image 'swarmpit/install:1.6' locally
1.6: Pulling from swarmpit/install
169185f82c45: Pull complete
34c29055ee42: Pull complete
29802c64cdfc: Pull complete
792ffc28964c: Pull complete
4d2d20f27c4d: Pull complete
3aa1579795b5: Pull complete
fea525433c1e: Pull complete
666eac166535: Pull complete
5b286e9f14be: Pull complete
3de9817c376c: Pull complete
827fa1909b70: Pull complete
f963bb249c55: Pull complete
Digest: sha256:e1dd2a6c47fc12b1dc62d6bd7d067742a16c511be190685c0f23d4c8a4c14ba7
Status: Downloaded newer image for swarmpit/install:1.6
                                        _ _
 _____      ____ _ _ __ _ __ ___  _ __ (_) |_
/ __\ \ /\ / / _` | '__| '_ ` _ \| '_ \| | __|
\__ \\ V  V / (_| | |  | | | | | | |_) | | |_
|___/ \_/\_/ \__,_|_|  |_| |_| |_| .__/|_|\__|
                                 |_|
Welcome to Swarmpit
Version: 1.6
Branch: 1.6

Preparing dependencies
latest: Pulling from byrnedo/alpine-curl
8e3ba11ec2a2: Pull complete
6522ab4c8603: Pull complete
Digest: sha256:e8cf497b3005c2f66c8411f814f3818ecd683dfea45267ebfb4918088a26a18c
Status: Downloaded newer image for byrnedo/alpine-curl:latest
DONE.

Preparing installation
Cloning into 'swarmpit'...
remote: Enumerating objects: 459, done.
remote: Counting objects: 100% (459/459), done.
remote: Compressing objects: 100% (210/210), done.
remote: Total 16251 (delta 204), reused 453 (delta 201), pack-reused 15792
Receiving objects: 100% (16251/16251), 4.28 MiB | 6.06 MiB/s, done.
Resolving deltas: 100% (9686/9686), done.
DONE.

Application setup
Enter stack name [swarmpit]:
Enter application port [888]:
Enter database volume driver [local]:
Enter admin username [admin]:
Enter admin password (min 8 characters long): <sanitized>
DONE.

Application deployment
Creating network swarmpit_net
Creating service swarmpit_agent
Creating service swarmpit_app
Creating service swarmpit_db
DONE.

Starting swarmpit..........DONE.
Initializing swarmpit...DONE.

Summary
Username: admin
Password: <sanitized>
Swarmpit is running on port :888

Enjoy :)
```

## Update

We already installed version 1.6 of Swarmpit with the first installation. Now the version 1.7 is available. How to update to that version?

First get the latest docker-compose file template from swarmpit github.

Replace the latest tag by the versions you want to install for the swarmpit-app and swarmpit-agent. The current latest versions are 2.1 for swarmpit-agent and 1.7 for swarmpit-app.

Also read the changelog for the release. For version 1.7, the agent should be updated to 2.1 and a label swarmpit.agent - true must be added to the service.

Logon on a swarm manager node and create the deploy file swarmpit-{version}.yml. Paste into this new file the adapted docker-compose file.

Then update the current swarmpit stack issuing the following command

```bash
sudo docker stack deploy -c swarmpit-1.7.yml swarmpit
Updating service swarmpit_db (id: 88whn9ike4xconb0ybxq5cj0m)
Updating service swarmpit_agent (id: ezuraofznc7ktzlhahgqxo9pb)
Updating service swarmpit_app (id: ixzkzcpjpacq53auya79dnazb)
```

