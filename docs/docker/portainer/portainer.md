# Portainer

Portainer is a GUI to manage docker / swarm nodes.

## Deploy Portainer in Swarm

Download the docker-compose YAML file describing the portainer stack

```bash
curl https://downloads.portainer.io/portainer-agent-stack.yml -o portainer-agent-stack.yml
```

Check the portainer-agent-stack.yml file (never apply something downloaded from internet without checking first).  
By default, the stack file declares a portainer-data volume. We will adapt that to use a bind volume on the host.

Apply the docker-compose stack definition.

```bash
docker stack deploy --compose-file=portainer-agent-stack.yml portainer
```