# Docker in production

This page assembles info about the minimum setup to run docker in a production environment.

## Scheduling

Running Docker in production should always be considered with the deployment of a scheduler. A scheduler enables to run Docker containers on multiple
hosts with self-healing and auto-scaling capabilities.

The most famous scheduler / orchestrator is Kubernetes (K8S) but it is also the most complex one.
It could be easily deployed through the Rancher platform but you won't have all the details about how to fix it in case it fails.

The other approach which is easier is to use Docker Swarm mode. Swarm mode enables multiple Docker hosts to work together in a swarm cluster.
One can then define and run services / stacks using an overlay network allowing containers to talk to each other on all the hosts part of the swarm.  
This is how I configured it for my seblab setup.

## Logging

Log management in Docker and especially with an orchestrator like Swarm or Kubernetes can quickly become a real problem. As containers are ephemerous, so are their logs and this can make troubleshooting a difficult task. A general good practice is to centralize logs produced by docker containers to a central location.

Interesting reading: [Top 10 logging issues in Docker](https://sematext.com/blog/top-10-docker-logging-gotchas)

There are multiple ways to send docker logs to Graylog. The most commonly used solution (especially also when working with Docker Swarm) is to use a container that will collect logs from Docker API to send them to a specific destination.  
There are 2 main products that can be used to fulfill this role :

* [Logspout](https://github.com/gliderlabs/logspout)

* [Sematext](https://sematext.com/docker/)

Both products are open source, uses the docker API to retrieve logs from containers and can deal with ansi escape characters (that can confuse log management systems).
Sematext also seems to offer a better support for multiline log massages and can collect metrics.

### Docker logging drivers

* **json**: default driver. Enables the use of docker logs command. Don't forget to specify log-max--size and log-rotation options in `/etc/docker/daemon.json`
* **gelf**: logs are generated in GELF format and sent to Graylog in UDP (only). The problem is that it works only in UDP and DNS resolution of the logging destination is only resolved when the container starts. Read the full story on [Jérome Petazzoni's blog](https://jpetazzo.github.io/2017/01/20/docker-logging-gelf)
* **journald**: to be tested

## Monitoring

Docker daemon metrics -- > Telegraf --> InfluxDB 

## Load-balancing

See [Traefik](https://traefik.io)

## Storage

This is the most tricky part of a Docker deployment on multiple hosts. If you only deploy stateless containers, then you can perfectly use
only local Docker volumes that will be deleted when the containers are stopped.

On the other hand, when some data must persist (e.g.: running a database in a container) we must use a distributed storage enabling the use of persistent volumes across multiple hosts. The difficulty lies in the setup and maintenance of a distributed file system (GlusterFS / CephFS).

Another option is to use labels to put constraints on deployments. Labels enables to deploy services to dedicated swarm nodes. It is of course a high constraint

## Docker Registry

### Install a private registry

[Docker Distribution](https://github.com/docker/distribution) is an open source software enabling to deploy a private Docker registry.


### Install a security / access service to the private registry

[Portus](http://port.us.org/) is also an open source project that secures and ease the access to your private Docker registry.

### Install a Docker image security scanner

[Portus](http://port.us.org/) can be configured to run a security scanner when a new Docker image is pushed into the private registry. The open source project [Claire](https://github.com/coreos/clair) is one of the most popular docker image security scanner tool.
