# AWX info

This page regroups all the information about AWX for seblab.be.

## Installation

Install base packages  
Install docker-ce stable  
Install python-docker  
Install python-docker-compose  

To install AWX, first check the instructions at [AWX installation docuemntation](https://github.com/ansible/awx/blob/devel/INSTALL.md)

Clone `awx` and `awx-logos` repository

```bash
$ git clone https://github.com/ansible/awx.git
$ git clone https://github.com/ansible/awx-logos.git
```

Adapt at least the pg_data_dir variable in the inventory file. It defaults to /tmp and that is not a good location for database files.

```bash
# Common Docker parameters
# postgres_data_dir=/tmp/pgdocker
postgres_data_dir=/var/lib/pgdocker
```

As we deploy using docker-compose, also check that section of the inventory file

```bash
# Docker Compose Install
# use_docker_compose=false
use_docker_compose=true
# The docker_compose.yml file will be created in this directory
# The name of the directory (here "awx") will be the prefix of the docker containers
docker_compose_dir=/var/lib/awx
```

Also specify an alternate DNS because otherwise AWX won't be able to find local hosts (t least in my lab)

```bash
# Container networking configuration
# Set the awx_task and awx_web containers' search domain(s)
#awx_container_search_domains=example.com,ansible.com
# Alternate DNS servers
#awx_alternate_dns_servers="10.1.2.3,10.2.3.4"
awx_alternate_dns_servers="192.168.20.10"
```

Then run the playbook from `./awx/installer` as root

```bash
# cd /home/seb/awx/installer
# ansible-playbook -i inventory install.yml
```

```bash
$ firewall-cmd --permanent --zone=public --add-service=http
$ firewall-cmd --permanent --zone=public --add-service=https
```

## Configuration

## Patterns / Best practices

### Projects

AWX Projects are logical collections of playbooks. These projects will be stored in a version control system (git).  
When imported, the projects are stored in /var/lib/awx/projects. In a standard deployment, the data resides in an implicit docker volume used by containers `awx_task` and `awx_web`.  
It is thus only accessible / visible through the running container.  
To map it to a file system on the host running the docker container, you need to modify the docker-compose file to include a volume mapping to `/var/lib/awx/projects`.

### Roles

Roles are not specific to AWX but to Ansible. A role is a reusable collection of actions to run to install / configure a software on a server.  
Each role will be stored in its own version control system repository (git).

### Naming conventions for roles / playbooks

Projects will be named `ansible-project-domain-application/usage`

Example: 
The project containing the playbook for the base installation of a server seblab.be will be named  
ansible-project-seblab-base

Roles will be named `ansible-role-domain-application/usage`

Example:
The role describing how to install graylog on a seblab server will be named
ansible-role-seblab-graylog

### Relationship between projects and roles

Projects, and thus the related playbooks, use roles to deploy software components and/or configure servers with Ansible.  
The roles can be managed in 3 different ways:

1. in a `./roles` subdirectory
2. using `git submodules`
3. using `ansible-galaxy` and `requirements.yml`

As described in this blog post https://www.ansible.com/blog/using-ansible-and-ansible-tower-with-shared-roles, the recommended way of doing this is by defining a subdirectory /roles in each project repository and inside create a requirements.yml file describing where the needed roles can be found.  
Each time a project is synchronized, AWX calls ansible-galaxy to find the roles described in the file `project_root/roles/requirements.yml`.  

The projects will be stored in `/var/lib/awx/projects`.  
The needed roles will be stored in `/var/lib/awx/projects/.../project_repo_name/roles/role_repo_name`.

## Migration

### Official procedure

Unfortunately, AWX does not support a direct upgrade through the recreation of containers. To migrate, one has to install a fresh new awx instance  and migrate data from the old to the new one. This section is largely based on the documentation about [data migration between AWX instances](https://github.com/ansible/awx/blob/devel/DATA_MIGRATION.md)

Migration occurs with the help of `tower-cli` tool.

```bash
$ pip install --upgrade ansible-tower-cli
```

Then configure `tower-cli`. The AWX host URL, user, and password must be set for the AWX instance to be exported:

```bash
$ tower-cli config host http://<old-awx-host.example.com>
$ tower-cli config username <user>
$ tower-cli config password <pass>
```

!!! Note
    Make sure to include http:// before host name in the host configuration variable because by default tower-cli uses https to connect to AWX / Tower.

!!! Info
    Disable ssl verification otherwise you'll receive an error

```bash
$ tower-cli config ssl_verify false
```

Export all data EXCEPT 

* Log / history
* Credentials passwords
* AWX / LDAP configurations

```bash
$ tower-cli receive --all > assets.json
```

Stop all containers as root and prune the volumes

```bash
# cd /var/lib/awxcomposer
# docker-compose stop
# docker system prune -a --volumes
```

Erase the PostgreSQL data directory.

```bash
# rm -rf /var/lib/pgdocker/*
```

Drop the modifications done in `inventory` file.

```bash
cd ~/awx
git checkout -- ./installer/inventory
``` 

Pull latest version of git repository

```bash
git pull
```

Adapt the inventory file again and run the install.yml ansible playbook.

Import the data with tower-cli

```bash
$ tower-cli send asset.json
```

Problems:

1. User admin cannot be imported.
   There was an error due to the fact that the backup json file had some data specified for the admin user and tower-cli is not able to update the admin user (surely because it uses it to connect to AWX).
   Solution:  
   Delete the data in the json file and restart the import
2. Projects cannot be synchronized from git repositories.
   This is normal because credentials are not restored (they are not part of the export json file). The synchronization from a private git repository will thus fail.
   Solution:  
   Enter valid credentials and synchronize the projects manually.
3. Job templates are not created.
   Normal because playbooks cannot be found due to the fact that they are not synchronized due to missing credentials.  
   Solution:  
   After credentials are replaced, recreate the templates.

#### Best practice with tower-cli

Use separate runs of tower-cli to generate json backup files for each type of resource that must be exported.
This enables to rerun some imports without having dependency issues.

### Non-official procedure

The non official procedure should work from release 2.1.
Stop all AWX containers.
Backup the PostgreSQL data directory.
Prune the docker volumes.

The script hereunder automates these actions

```bash

BUDIR=/opt/ansible_backups/$(date '+%d.%m.%Y_%T')

# docker stop $(docker ps -a -q)
docker-compose stop
mkdir -p $BUDIR
cp -r /var/lib/awx /var/lib/pgdocker "$BUDIR"

docker system prune -a --volumes
```

Download new awx zip, adapt inventory, run the playbook. Done.
