# Install ZABBIX on seblab zabbix host

Add zabbix repo

```bash
sudo rpm -ivh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-1.el7.noarch.rpm
```

Install zabbix server with MySQL support

```bash
sudo yum install zabbix-server-mysql
```

Install zabbix frontend with MySQL support

```bash
sudo yum install zabbix-web-mysql
```

Install mariadb-server on zabbix host

```bash
sudo yum install mariadb-server
sudo systemctl enable mariadb
sudo systemctl start mariadb
```

Harden MariaDB installation

```bash
sudo mysql_secure_installation
```

Create the MySQL database & user
Connect to mysql as root

```bash
mysql -uroot -p<password>
```

Create the zabbix database with utf8

```bash
create database zabbix character set utf8 collate utf8_bin;
```

Create the zabbix user

```bash
grant all privileges on zabbix.* to zabbix@localhost identified by '<password>';
```

Load database


Edit the zabbix configuration file and set the db details

```bash
vi /etc/zabbix/zabbix_server.conf

DBHost=localhost
DBName=zabbix
DBUser=zabbix
DBPassword=<password>
```

Start and enable zabbix-server

```bash
sudo systemctl start zabbix-server
sudo systemctl enable zabbix-server
```


Does not start
Error is cannot start alert manager service: Cannot bind socket to "/var/run/zabbix/zabbix_server_alerter.sock": [13] Permission denied.

This is caused by SELinux
To avoid that error, disable SElinux (not preferred) 

```bash
sudo vi /etc/sysconfig/selinux
SELINUX=disabled
```

or

```bash
grep "comm.*zabbix_server.*zabbix_t" /var/log/audit/audit.log | audit2allow -M comm_zabbix_server_zabbix_t
semodule -i comm_zabbix_server_zabbix_t.pp
```

Edit /etc/httpd/conf.d/zabbix.conf and uncomment the “date.timezone” setting and set the right timezone for you. 

```bash
php_value date.timezone Europe/Brussels
```

If SELinux enforced

```bash
sudo setsebool -P httpd_can_connect_zabbix on
```

Configure firewall for zabbix web access
`firewall-cmd --permanent --zone=public --add-port=80/tcp`

Start httpd

```bash
sudo systemctl start httpd
```

Setup zabbix via the web interface https://www.zabbix.com/documentation/4.0/manual/installation/install#installing_frontend

Install agent

sudo yum install zabbix-agent

Configure firewall

sudo firewall-cmd --permanent --zone=public --add-port=10050/tcp
success
sudo firewall-cmd --permanent --zone=public --add-port=10051/tcp
success
sudo firewall-cmd --reload
