# SEBLAB.BE

This is the documentation about my setup on my [SOYOUSTART](http://soyoustart.com) infrastructure.

## Building the documentation

This documentation is written in Markdown and build with Mkdocs and the Material theme. All documents are stored in a git repository hosted on Bitbucket.

### Workflow

Git clone / pull  ---->  Build documentation with Docker container ----> Build Docker container to serve the doc ----> Deploy / update Docker swarm service with the documentation.

All these actions are orchestrated by Jenkins.

Docker container image for the build is [squidfunk/mkdocs-material](https://hub.docker.com/r/squidfunk/mkdocs-material/).

Build the documentation with the following command:

```bash
docker run --rm -v ${PWD}:/docs squidfunk/mkdocs-material build
```

!!! error
    For Jenkins to not fail with an error while invoking docker, add the jenkins user to docker group with the command  
    `usermod -aG docker jenkins`


!!! Tip
    Don't forget to restart Jenkins before trying again the job to update the security context.


As the docker container to build the documentation is running as root, the directory $WORKSPACE/site is owned by root. To change ownership, one must execute a build step as root with sudo.
To run a task with sudo in jenkins, add the following line in the file /etc/sudoer through the command visudo :

```bash
jenkins ALL=(root) NOPASSWD: ALL
```

This enables user jenkins on ALL hosts to run ALL commands as root without password.

---

## Server infrastructure

### Charon

OS: FreeBSD - pfSense  
IP: 192.168.20.1  
Role: firewall / VPN / Gateway  

### Cerberus

OS: CentOS  
IP: 192.168.20.10  
Role: reverse-proxy Traefik  

### db

OS: CentOS  
IP: 192.168.20.30  
Role: MySQL (or other) database host  

### Graylog

OS: CentOS  
IP: 192.168.20.20  
Role: Graylog all in one

More Graylog info in the [Graylog section](./graylog/graylog.md) of this documentation.

### Tools

OS: CentOS  
IP: 192.168.20.22  
Role(s): Gitea, [Jenkins](http://jenkins.io)  

Configure firewalld for gitea  
`firewall-cmd --permanent --zone=public --add-port=3000/tcp`  
and Jenkins  
`firewall-cmd --permanent --zone=public --add-port=8080/tcp`  
and GoCD

More Jenkins info in the [Jenkins section](./jenkins/jenkins.md) of this documentation.
More git info in the [git section](./git/git.md) of this documentation.
More GoCD info in the [GoCD section](./gocd/gocd.md) of this documentation.

### AWX

AWX is not used anymore in the current seblab infrastructure. Keeping this section for reference.

OS: CentOS  
IP: 192.168.20.23  
Role: Ansible AWX installation

More AWX info in the [AWX section](./awx/awx.md) of this repository.
For Ansible related tips and tricks, consult the [Ansible section](./ansible/ansible.md) of this documentation.

### SWARM01

OS: CentOS  
IP: 192.168.20.40  
Role: Docker Swarm node 01 - Manager & Worker

Ansible roles deployed (in addition to the standard ones)

* ansible-role-docker

### RANCHER

OS: CentOS
IP: 192.168.20.50
Role: rancher master node

Ansible roles deployed (in addition to the standard ones)

* ansible-role-docker

Rancher is a kubernetes cluster manager. To get started, have a look at [rancher quick start guides](https://rancher.com/docs/rancher/v2.x/en/quick-start-guide/)

## Actions to do when installing a new VM

Install base VM with CentOS 7 Minimal ISO  
Configure

* Language
* Keyboard
* Timezone
* IPv4 address - must be fixed manually  
  Gateway: 192.168.20.1
  DNS: 192.168.20.10  
  FQDN: \<hostname\>.seblab.local
* Give password to user `root` and create user `seb` as administrator

Add the server to seblab ansible inventory file then run the following ansible roles :

* ansible-role-sshkeys
* ansible-role-update-centos
* ansible-role-seblab-base
* ansible-role-hostname : before running this role, adapt the hosts.j2 template to include the new server IPv4 address and hostnames.  
  Run this role on **ALL** hosts in the ansible inventory file.
* ansible-role-resolvconf : this playbook **MUST NOT** run on cerberus - 192.168.20.10 othewise external name resolution won't work anymore.
* ansible-role-rsyslogconfig
* ansible-role-seblab-install-ca-certs. Run this role on **ALL** hosts in the ansible inventory file.
* ansible-role-seblab-telegraf

Sysdig ?

To configure network card : nmtui (network manager text user interface)
