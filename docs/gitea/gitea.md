# Gitea documentation

## Backups

Gitea backups will be stored in azure blob storage

On a computer with AZ CLI installed

First login to your azure account

```bash
az login
```

Create a resource group if you have none already. It should be created in location francecentral.

```bash
az group create --name seblab-resource-group --location francecentral
{
  "id": "/subscriptions/bcb5f187-d1a0-40cd-b6ed-8ca6739825fd/resourceGroups/seblab-resource-group",
  "location": "francecentral",
  "managedBy": null,
  "name": "seblab-resource-group",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null,
  "type": null
}
```

Next create a storage account to store the backups of seblab infrastructure

```bash
az storage account create --name seblabbackup --resource-group seblab-resource-group --location francecentral --sku Standard_LRS --kind StorageV2
{
  "accessTier": "Hot",
  "creationTime": "2019-05-04T08:20:00.367163+00:00",
  "customDomain": null,
  "enableAzureFilesAadIntegration": null,
  "enableHttpsTrafficOnly": false,
  "encryption": {
    "keySource": "Microsoft.Storage",
    "keyVaultProperties": null,
    "services": {
      "blob": {
        "enabled": true,
        "lastEnabledTime": "2019-05-04T08:20:00.429629+00:00"
      },
      "file": {
        "enabled": true,
        "lastEnabledTime": "2019-05-04T08:20:00.429629+00:00"
      },
      "queue": null,
      "table": null
    }
  },
  "failoverInProgress": null,
  "geoReplicationStats": null,
  "id": "/subscriptions/bcb5f187-d1a0-40cd-b6ed-8ca6739825fd/resourceGroups/seblab-resource-group/providers/Microsoft.Storage/storageAccounts/seblabbackup",
  "identity": null,
  "isHnsEnabled": null,
  "kind": "StorageV2",
  "lastGeoFailoverTime": null,
  "location": "francecentral",
  "name": "seblabbackup",
  "networkRuleSet": {
    "bypass": "AzureServices",
    "defaultAction": "Allow",
    "ipRules": [],
    "virtualNetworkRules": []
  },
  "primaryEndpoints": {
    "blob": "https://seblabbackup.blob.core.windows.net/",
    "dfs": "https://seblabbackup.dfs.core.windows.net/",
    "file": "https://seblabbackup.file.core.windows.net/",
    "queue": "https://seblabbackup.queue.core.windows.net/",
    "table": "https://seblabbackup.table.core.windows.net/",
    "web": "https://seblabbackup.z28.web.core.windows.net/"
  },
  "primaryLocation": "francecentral",
  "provisioningState": "Succeeded",
  "resourceGroup": "seblab-resource-group",
  "secondaryEndpoints": null,
  "secondaryLocation": null,
  "sku": {
    "capabilities": null,
    "kind": null,
    "locations": null,
    "name": "Standard_LRS",
    "resourceType": null,
    "restrictions": null,
    "tier": "Standard"
  },
  "statusOfPrimary": "available",
  "statusOfSecondary": null,
  "tags": {},
  "type": "Microsoft.Storage/storageAccounts"
}
```

In this storage account, we will create our first blob container to store gitea's backup

```bash
az storage container create --name gitea --fail-on-exist --account-name seblabbackup
{
  "created": true
}
```

We will use [azcopy](https://docs.microsoft.com/en-us/azure/storage/common/storage-use-azcopy-v10) to transfer the backup from the local filesystem to azure blob storage.

To enable azcopy to access our azure storage container, we will not use our storage key account but we'll generate a [SAS token](https://docs.microsoft.com/en-us/azure/storage/common/storage-dotnet-shared-access-signature-part-1).

```bash
az storage container generate-sas --name gitea --account-name seblabbackup
"sv=2018-03-28&sr=c&sig=oJxJ%2B9uJ7Tb2cDHdEvDdWt9blw5OxmmM05Y0kga2ogc%3D"
```

