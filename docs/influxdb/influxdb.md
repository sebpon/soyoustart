# Deploy InfluxDB / Grafana in docker swarm

This document describes how to deploy influxdb / chronograf and grafana in a docker swarm node (standalone).

First, enable the following firewall rules:

```bash
firewall-cmd --permanent --zone=public --add-port=3000/tcp  
firewall-cmd --permanent --zone=public --add-port=8086/tcp  
firewall-cmd --permanent --zone=public --add-port=8888/tcp  
```

!!! note
    Looks like it is not needed when deploying to SWARM. Without adding these rules explicitely I was able to contact Chronograf and Grafana on the Swarm IP address. To be verified.

## Deploy InfluxDB in docker swarm

!!! info
    Prerequisite: there should be a docker swarm available.

We will deploy the following components in the swarm cluster as a stack

* influxdb alpine version 1.7.2
* chronograf alpine version 1.7.5 - docker tag 1.5.5-alpine
* grafana version 5.4.2

Even if we currently have only one node in the swarm, we'll add a label the force the deployment to happen on a specific swarm node. As we are not using shared storage, we have to force the deployment of the containers always on the same node otherwise we'll loose our data if it gets scheduled on another node in the swarm.  
To enforce the deployment on a specific swrm node, we'll make use of a node label.  
Each node participating in a swarm cluster can have labels applied to him. A label is a key/value pair.  
Let's create a label influxdb=true on the node that we will prepare for influxdb.

`docker node update --label-add influxdb=true <node-id>`

Check that the label is applied using

`docker node inspect <node-id> --pretty`

Example:

```bash
docker node update --label-add influxdb=true qd4nv2
qd4nv2
[root@swarm01 ~]# docker node inspect qd --pretty
ID:                     qd4nv2wmhp609scw1s8kmbi6h
Labels:
 - influxdb=true
Hostname:               swarm01.seblab.local
Joined at:              2019-01-06 14:12:46.696625398 +0000 utc
Status:
 State:                 Ready
 Availability:          Active
 Address:               192.168.20.40
Manager Status:
 Address:               192.168.20.40:2377
 Raft Status:           Reachable
 Leader:                Yes
Platform:
 Operating System:      linux
 Architecture:          x86_64
Resources:
 CPUs:                  2
 Memory:                1.795GiB
Plugins:
 Log:           awslogs, fluentd, gcplogs, gelf, journald, json-file, local, logentries, splunk, syslog
 Network:               bridge, host, macvlan, null, overlay
 Volume:                local
Engine Version:         18.09.0
```

!!! tips "Prerequisites for grafana in docker using bind mount"
    Grafana versions >= 5.1 do not run with root user anymore. Processes are run with user grafana (uid 472) member of group grafana (gid 472).  
    When using bind mount type volumes for /var/lib/grafana, the directory on the docker node where we bind to must have correct authorizations otherwise the grafana container cannot start.

Create the grafana group with gid 472 (example with ansible)
`ansible -i <inventory file> -m group -a "name=grafana state=present gid=472" <host inventory name> -K -b`

Create the grafana user with uid 472 and member of grafana group (example with ansible)
`ansible -i <inventory file> -m user -a "name=grafana state=present uid=472 group=grafana" <host inventory name> -K -b`

Prepare the directories for bind mounts on the manager node

```bash
sudo mkdir -p /data/chronograf
sudo mkdir -p /data/influxdb
sudo mkdir -p /data/grafana
sudo chown -R grafana:grafana /data/grafana
```

!!! note
    This can be prepared with an ansible role / playbook

We'll use a docker-compose file to simplify the deployment of the services composing the stack.
Create the docker-compose.yml stack file with the following content :

Deploy the stack TICK using the following command on the swarm manager node

`docker stack deploy -c docker-compose.yml TICK`

Check the deployment

```bash
sudo docker stack ls
NAME                SERVICES            ORCHESTRATOR
TICK                3                   Swarm

sudo docker stack ps TICK
ID                  NAME                 IMAGE                     NODE                   DESIRED STATE       CURRENT STATE            ERROR                       PORTS
1s1umq9mmr8u        TICK_grafana.1       grafana/grafana:5.4.2     swarm01.seblab.local   Running             Running 9 minutes ago
kbua9klltc7f        TICK_chronograf.1    chronograf:1.7.5-alpine   swarm01.seblab.local   Running             Running 20 minutes ago
2n6g4gp3426t        TICK_influxdb.1      influxdb:1.7.2-alpine     swarm01.seblab.local   Running             Running 21 minutes ago
```

## InfluxDB configuration

Run influxdb client from the influxdb container.  
First determine the name of the container 

```bash
sudo docker container ls
CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS              PORTS               NAMES
c2a30ec422aa        grafana/grafana:5.4.2     "/run.sh"                16 minutes ago      Up 16 minutes       3000/tcp            TICK_grafana.1.j9qsd1oyc2yowgnceqw7rrosx
e354bb71017c        chronograf:1.7.5-alpine   "/entrypoint.sh chro…"   16 minutes ago      Up 16 minutes       8888/tcp            TICK_chronograf.1.zq6ctep5458hxnzh641inxsim
d57256578626        influxdb:1.7.2-alpine     "/entrypoint.sh infl…"   16 minutes ago      Up 16 minutes       8086/tcp            TICK_influxdb.1.xjjidbym7wlpxn137r9ar9mvv
```

Then start influx client in container influxdb (ID:d57256578626)

```bash
sudo docker container exec -it d572 influx
```
Logon in the influx instance with the admin user / password defined in the docker-compose file used to deploy the stack.

```sql
AUTH
username: admin
password:
SHOW DATABASES
name: databases
name
----
_internal

```

Create a telegraf database

```sql
CREATE DATABASE "telegraf"
```

Create a retention policy of 15 days that will be the default retention policy

```sql
CREATE RETENTION POLICY "fifteen_days" ON "telegraf" DURATION 15d REPLICATION 1 DEFAULT
```

Create a telegraf user and give himm all accesses on the database

```sql
CREATE USER "telegraf" WITH PASSWORD 'password'
GRANT all ON telegraf TO telegraf
```

Test the user  
```bash
curl -G http://<host-ip>:8086/query -u admin:secretpassword --data-urlencode "q=SHOW DATABASES"
{"results":[{"statement_id":0,"series":[{"name":"databases","columns":["name"],"values":[["_internal"],["telegraf"]]}]}]}
```

## Influxdb backup

Create a /data/backup directory as root on the docker swarm node hosting influxdb container.

```bash
sudo mkdir -p /data/influxdb/backup
```

Find the id of the container that runs influxdb

```bash
sudo docker container ls
CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS              PORTS                          NAMES
daaeb19d95ac        couchdb:2.3.0             "tini -- /docker-ent…"   About an hour ago   Up About an hour    4369/tcp, 5984/tcp, 9100/tcp   swarmpit_db.1.rds401s5xezgbfg24gyvne63g
cb7baa30a7dc        swarmpit/swarmpit:1.7     "java -jar swarmpit.…"   About an hour ago   Up About an hour    8080/tcp                       swarmpit_app.1.levwile4hro2b7gionllj17rm
ddda5605edaa        swarmpit/agent:2.1        "./agent"                About an hour ago   Up About an hour    8080/tcp                       swarmpit_agent.qd4nv2wmhp609scw1s8kmbi6h.s73uz236fbr5ty94shrs5scdi
b01c5c2553cc        grafana/grafana:6.2.2     "/run.sh"                2 hours ago         Up 2 hours          3000/tcp                       TICK_grafana.1.9jqinecjvh2nndugj5hs6vxcm
e354bb71017c        chronograf:1.7.5-alpine   "/entrypoint.sh chro…"   4 months ago        Up 4 months         8888/tcp                       TICK_chronograf.1.zq6ctep5458hxnzh641inxsim
d57256578626        influxdb:1.7.2-alpine     "/entrypoint.sh infl…"   4 months ago        Up 4 months         8086/tcp                       TICK_influxdb.1.xjjidbym7wlpxn137r9ar9mvv
```

Here the id is d57256578626.

Start an interactive command in the influxdb container to backup to /var/lib/influxdb/backup. It should be mapped to /data/influxdb/backup.

```bash
sudo docker container exec -it d572 influxd backup -database telegraf -portable /var/lib/influxdb/backup

2019/06/08 09:33:49 backing up metastore to /var/lib/influxdb/backup/meta.00
2019/06/08 09:33:49 backing up db=telegraf
2019/06/08 09:33:49 backing up db=telegraf rp=fifteen_days shard=272 to /var/lib/influxdb/backup/telegraf.fifteen_days.00272.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:50 backing up db=telegraf rp=fifteen_days shard=274 to /var/lib/influxdb/backup/telegraf.fifteen_days.00274.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:50 backing up db=telegraf rp=fifteen_days shard=276 to /var/lib/influxdb/backup/telegraf.fifteen_days.00276.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:50 backing up db=telegraf rp=fifteen_days shard=278 to /var/lib/influxdb/backup/telegraf.fifteen_days.00278.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:51 backing up db=telegraf rp=fifteen_days shard=280 to /var/lib/influxdb/backup/telegraf.fifteen_days.00280.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:51 backing up db=telegraf rp=fifteen_days shard=282 to /var/lib/influxdb/backup/telegraf.fifteen_days.00282.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:51 backing up db=telegraf rp=fifteen_days shard=284 to /var/lib/influxdb/backup/telegraf.fifteen_days.00284.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:51 backing up db=telegraf rp=fifteen_days shard=286 to /var/lib/influxdb/backup/telegraf.fifteen_days.00286.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:52 backing up db=telegraf rp=fifteen_days shard=288 to /var/lib/influxdb/backup/telegraf.fifteen_days.00288.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:52 backing up db=telegraf rp=fifteen_days shard=290 to /var/lib/influxdb/backup/telegraf.fifteen_days.00290.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:52 backing up db=telegraf rp=fifteen_days shard=292 to /var/lib/influxdb/backup/telegraf.fifteen_days.00292.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:53 backing up db=telegraf rp=fifteen_days shard=294 to /var/lib/influxdb/backup/telegraf.fifteen_days.00294.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:53 backing up db=telegraf rp=fifteen_days shard=296 to /var/lib/influxdb/backup/telegraf.fifteen_days.00296.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:54 backing up db=telegraf rp=fifteen_days shard=298 to /var/lib/influxdb/backup/telegraf.fifteen_days.00298.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:54 backing up db=telegraf rp=fifteen_days shard=300 to /var/lib/influxdb/backup/telegraf.fifteen_days.00300.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:55 backing up db=telegraf rp=fifteen_days shard=302 to /var/lib/influxdb/backup/telegraf.fifteen_days.00302.00 since 0001-01-01T00:00:00Z
2019/06/08 09:33:55 backup complete:
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.meta
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s272.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s274.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s276.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s278.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s280.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s282.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s284.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s286.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s288.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s290.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s292.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s294.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s296.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s298.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s300.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.s302.tar.gz
2019/06/08 09:33:55     /var/lib/influxdb/backup/20190608T093349Z.manifest
```

Check that the backup files are available on the host

```bash
sudo ls -l /data/influxdb/backup/
total 84948
-rw-------. 1 root root    3234 Jun  8 11:33 20190608T093349Z.manifest
-rw-r--r--. 1 root root    2220 Jun  8 11:33 20190608T093349Z.meta
-rw-------. 1 root root 5588866 Jun  8 11:33 20190608T093349Z.s272.tar.gz
-rw-------. 1 root root 5596827 Jun  8 11:33 20190608T093349Z.s274.tar.gz
-rw-------. 1 root root 5647643 Jun  8 11:33 20190608T093349Z.s276.tar.gz
-rw-------. 1 root root 5648075 Jun  8 11:33 20190608T093349Z.s278.tar.gz
-rw-------. 1 root root 5631221 Jun  8 11:33 20190608T093349Z.s280.tar.gz
-rw-------. 1 root root 5650938 Jun  8 11:33 20190608T093349Z.s282.tar.gz
-rw-------. 1 root root 5671668 Jun  8 11:33 20190608T093349Z.s284.tar.gz
-rw-------. 1 root root 5593489 Jun  8 11:33 20190608T093349Z.s286.tar.gz
-rw-------. 1 root root 5616133 Jun  8 11:33 20190608T093349Z.s288.tar.gz
-rw-------. 1 root root 5626082 Jun  8 11:33 20190608T093349Z.s290.tar.gz
-rw-------. 1 root root 5629701 Jun  8 11:33 20190608T093349Z.s292.tar.gz
-rw-------. 1 root root 5609445 Jun  8 11:33 20190608T093349Z.s294.tar.gz
-rw-------. 1 root root 5611494 Jun  8 11:33 20190608T093349Z.s296.tar.gz
-rw-------. 1 root root 5593610 Jun  8 11:33 20190608T093349Z.s298.tar.gz
-rw-------. 1 root root 5591207 Jun  8 11:33 20190608T093349Z.s300.tar.gz
-rw-------. 1 root root 2645572 Jun  8 11:33 20190608T093349Z.s302.tar.gz
```

## Issues

### Tag values deleted but still present in SHOW TAG VALUES query

This is a known issue in Influxdb. Tag values from deleted data are still displayed when running a SHOW TAG VALUES query. This is particularly annoying when using this kind of query to present a list of host in Grafana because it displays hosts that have been deleted.

The solution is to transform the query by adding a WHERE clause

```bash
SHOW TAG VALUES WITH KEY = "some_tag" WHERE some_tag != ''
```
