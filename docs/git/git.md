# Git tips

## Create a branch

!!! warning
    You should avoid to modify / work directly on your master branch. Instead you should create branches.

This is as easy as doing  
`git branch <branch name>`

## Using a branch

To switch to use a branch use  
`git checkout <branch name>`

## Checking on which branch we're working on

`git status`

## Merging a branch with `master`

First checkout to `master`  
`git checkout master`

Then merge the modification of the branch \<branch name\>  
`git merge <branch name>`

## Deleting a branch

To delete a local branch  
`git branch -d <branch name>`

This command will only delete a branch if it has been merged into `master`  
Use `-D` to delete with force mode

To delete a remote branch  
`git push --delete <branch name>`

## Checkthe URL of the remote repository

`git remote get-url origin`    
or  
`git remote -v`  

## Change the URL of the remote repository

`git remote set-url origin http://your.url.here/repo.git` 

## Update a submodule

If you work with `git submodules` then sometimes you need to update the submodule link in a repository.

Use the following command in the repository using the submodules to update the modules

`git submodule foreach git pull origin master`
