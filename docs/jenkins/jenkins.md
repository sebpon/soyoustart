# Jenkins

Jenkins is mainly used as an alternative to Ansible Tower / AWX. The idea is  to use it as a GUI to enable people to run Ansible jobs.

!!! Note 
    Do not forget that [Atlassian Bamboo](https://www.atlassian.com/software/bamboo) is the selected CI/CD tool for Sales Solutions builds & deployments.

## Jenkins Plugins

The following plugins ar of interest for Jenkins

* Blue Ocean: Blue Ocean is a new user interface that enables better visualization for Jenkins Pipelines

* Ansible: enables to use ansible tasks in Jenkins jobs / pipelines

* AnsiColors: to enable colors in text outputs (handy for ansible output logs)

## Configure Jenkins to run Ansible playbooks

Jenkins -> Manage Jenkins -> Global Tool Configuration  
Fill in the path to the directory where ansible executable is stored.

## Deploy user

By default, playbooks run through jenkins will connect to remote hosts as user jenkins (user which runs the jenkins process).  
Some playbooks must run as root by using the `become` feature. If we don't configure things differently, it means that we have to create a user jenkins on all our infrastructure that will be able to connect through SSH without password and become root as well because the ansible plugin for jenkins does not allow to specify a credential for the `become` feature.

We will take another path and configure a user named deploy on all hosts. This user will be member of the `wheel` group and will be able to run sudo without password. We will allow user jenkins to connect as deploy through SSH without password by adding the public key of the jenkins user to the allowed keys of the deploy user. 

We configure the remote_user variable in the file ansible.cfg of all our ansible projects. We can also do this on all jenkins build agents by modifying their ansible.cfg file.

Configure ssh keys for user jenkins

Start a shell as user jenkins

> This can only be run as root

```bash
sudo su -s /bin/bash jenkins
```

Check that you are in /var/lib/jenkins.

```bash
pwd
```

If not, switch to /var/lib/jenkins

```bash
cd /var/lib/jenkins
```

Create an ssh key pair

```bash
ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/var/lib/jenkins/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /var/lib/jenkins/.ssh/id_rsa.
Your public key has been saved in /var/lib/jenkins/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:wvdDvKcqcwXOOcZcFOa3Y9a2Z9vij3gdYCmQn7PwrMg jenkins@tools.seblab.local
The key's randomart image is:
+---[RSA 2048]----+
|          +.     |
|         =.      |
|         .+ o .  |
|     .  .o.* *   |
|      o=S+* X +  |
|       oOo.B o o |
|      ...o= . ..+|
|      oE.. + .o++|
|       +... .oo+o|
+----[SHA256]-----+
```

Then run the following playbook from jenkins host as root or jenkins (otherwise you won't get access to the file /var/lib/jenkins/.ssh/id_rsa.pub)

```yaml
---
# Playbook to create the deploy user for ansible deployments
- hosts: all
  become: true

  tasks:
    - name: create user deploy
      user:
        name: deploy
        group: wheel
        shell: /bin/bash
        state: present
    
    - name: add jenkins public key to authorized_keys of user deploy
      authorized_key:
        user: deploy
        state: present
        key: "{{ lookup('file', '/var/lib/jenkins/.ssh/id_rsa.pub') }}"
    
    - name: enable deploy user to become root without password
      copy: content="deploy ALL=(ALL) NOPASSWD:ALL" dest="/etc/sudoers.d/deploy" owner=root group=root mode=0440
...
```

To run the playbook, create 2 files

1) /var/lib/jenkins/inventory_seblab : this file contains all hosts of seblab infrastructure
2) /var/lib/jenkins/ansible.cfg : this file contains the options remote_user = seb (already known on all hosts) and host_key_checking = False

Run the playbook as user jenkins

```bash
ansible-playbook -i ./inventory_seblab -k -K playbook_create_user_deploy.yml
SSH password:
SUDO password[defaults to SSH password]:

PLAY [all] *****************************************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************************************
ok: [tools.seblab.local]
ok: [db.seblab.local]
ok: [graylog.seblab.local]
ok: [awx.seblab.local]
ok: [zabbix.seblab.local]
ok: [docs.seblab.local]
ok: [swarm01.seblab.local]
ok: [cerberus.seblab.local]

TASK [create user deploy] **************************************************************************************************************************************************************
changed: [db.seblab.local]
changed: [awx.seblab.local]
changed: [zabbix.seblab.local]
changed: [graylog.seblab.local]
changed: [tools.seblab.local]
changed: [swarm01.seblab.local]
changed: [docs.seblab.local]
changed: [cerberus.seblab.local]

TASK [add jenkins public key to authorized_keys of user deploy] ************************************************************************************************************************
changed: [graylog.seblab.local]
changed: [db.seblab.local]
changed: [zabbix.seblab.local]
changed: [awx.seblab.local]
changed: [tools.seblab.local]
changed: [docs.seblab.local]
changed: [swarm01.seblab.local]
changed: [cerberus.seblab.local]

PLAY RECAP *****************************************************************************************************************************************************************************
awx.seblab.local           : ok=3    changed=2    unreachable=0    failed=0
cerberus.seblab.local      : ok=3    changed=2    unreachable=0    failed=0
db.seblab.local            : ok=3    changed=2    unreachable=0    failed=0
docs.seblab.local          : ok=3    changed=2    unreachable=0    failed=0
graylog.seblab.local       : ok=3    changed=2    unreachable=0    failed=0
swarm01.seblab.local       : ok=3    changed=2    unreachable=0    failed=0
tools.seblab.local         : ok=3    changed=2    unreachable=0    failed=0
zabbix.seblab.local        : ok=3    changed=2    unreachable=0    failed=0
```

Now test from user jenkins if you can connect to all hosts as user deploy.

Change the remote_user in /var/lib/jenkins/ansible.cfg to deploy and try the following command

```bash
ansible -i ./inventory_seblab -m ping all
awx.seblab.local | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
zabbix.seblab.local | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
db.seblab.local | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
graylog.seblab.local | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
tools.seblab.local | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
swarm01.seblab.local | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
cerberus.seblab.local | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
docs.seblab.local | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```