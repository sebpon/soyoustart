# SEBLAB.BE

This repository contains all the documentation about my setup on my [SOYOUSTART](http://soyoustart.com) infrastructure.

The documentation is now built with MkDocs and hosted on my own [seblab.be docs site](https://docs.seblab.be)